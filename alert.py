from pystalk import BeanstalkClient
import re,requests,json, telegram
from ConfigParser import ConfigParser
from bs4 import BeautifulSoup

___author___  = "Kenj"

class alert:
    def __init__(self):
        self.conf   = ConfigParser()
        self.conf.read("config.conf")
        self.fromnsqph  = ['sc_instagram','sc_facebook','sc_youtube']
        self.fromnsqndc = ['online-news','printed-news','tv-news']
        self.ipbeanstalk = dict(self.conf.items('ip_beanstalk'))
        self.apinsq = dict(self.conf.items('api_nsq'))
        self.cons_list  = dict(self.conf.items('kafka_consumer_list'))


    def fromkafka(self,resultfinal = []):
        consumer    = []
        split = self.cons_list['list_checker']
        splitter = str(split).split(',')
        judul   = "\n*-- \t KAFKA \t --*"
        for a in range(splitter.__len__()):
            saus    = self.conf.get('kafka_consumer_list', 'url_source').format(splitter[a])
            result = []
            reqsaus = requests.get(saus)
            soup    = BeautifulSoup(reqsaus.content,'html.parser')
            consumers   = "\n*{0}*".format(splitter[a])
            consumer.append(consumers)
            for b in range(soup.find_all('tr').__len__()-1):
                b+=1
                filt    = re.sub('<.*?>','',str(soup.find_all('tr')[b]))
                filt2   = re.sub(' ','',filt)
                splitter2   = filt2.split("\n")
                for c in range(4):
                    if splitter2[c] is '':
                        splitter2.pop(c)
                        continue
                if int(splitter2[2]) <= 0: continue
                else:
                    reskafka = "{0} : {1}".format(splitter2[0], splitter2[2])
                    result.append(reskafka)
            resultfinal.append(result)
        return judul,consumer,resultfinal

    def frombeanstalk(self,result = []):
        a   = 0
        judul   = "\n*-- \t BEANSTALK \t --*"
        while True:
            tw  = re.compile('sc_twitter_')
            fb  = re.compile('sc_fb_')
            ig  = re.compile('sc_ig_')
            yt  = re.compile('sc_youtube_')
            if a == 1:
                tube_list_fb    = []
                tube_list_ig    = []
                tube_list_yt    = []
                client  = BeanstalkClient(self.ipbeanstalk['ip_ph'],self.ipbeanstalk['port_ph'])
                list_tube   = client.list_tubes()
                for b in range(list_tube.__len__()):
                    if fb.findall(list_tube[b]):
                        tube_list_fb.append(list_tube[b])
                    if ig.findall(list_tube[b]):
                        tube_list_ig.append(list_tube[b])
                    if yt.findall(list_tube[b]):
                        tube_list_yt.append(list_tube[b])
                tube_list   = tube_list_fb + tube_list_ig +  tube_list_yt
                for c in range(tube_list.__len__()):
                    data    = client.stats_tube(tube_list[c])
                    if data['current-jobs-urgent'] >= 3000 or data['current-jobs-ready'] >= 3000:
                        res = "\n*{0}*\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}\njobs-reserved: {5}"\
                            .format(data['name'],data['current-watching'],data['current-jobs-urgent'],data['current-jobs-ready'],data['current-jobs-buried'],data['current-jobs-reserved'])
                        result.append(res)
                    else: continue
                break
            else:
                tube_list   = []
                client  = BeanstalkClient(self.ipbeanstalk['ip_bintaro'], self.ipbeanstalk['port_bintaro'])
                list_tube   = client.list_tubes()
                for b in range(list_tube.__len__()):
                    if tw.findall(list_tube[b]): tube_list.append(list_tube[b])
                for c in range(tube_list.__len__()):
                    data    = client.stats_tube(tube_list[c])
                    if data['current-jobs-urgent'] >= 3000 or data['current-jobs-ready'] >= 3000:
                        res = "\n*{0}*\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}\njobs-reserved: {5}"\
                            .format(data['name'],data['current-watching'],data['current-jobs-urgent'],data['current-jobs-ready'],data['current-jobs-buried'],data['current-jobs-reserved'])
                        result.append(res)
                    else: continue
            a += 1
        return judul,result

    def fromnsq(self, result = []):
        a   = 0
        judul = "\n*--\t NSQ \t--*"
        while True:
            if a == 0:
                #NSQ PH
                for b in self.fromnsqph:
                    req = requests.get(self.apinsq['api_nsq_ph'].format(b))
                    data    = json.loads(req.content)
                    depth       = data['depth']
                    conektion   = len(data['clients'])
                    if depth != 0:
                        self.botmesseg ="*\n{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(data['topic_name'],data['channel_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
            elif a == 1:
                #NSQ NDC
                for c in self.fromnsqndc:
                    req     = requests.get(self.apinsq['api_nsq_ndc'].format(c))
                    data = json.loads(req.content)
                    depth = data['depth']
                    conektion   = len(data['clients'])
                    if depth != 0:
                        self.botmesseg = "*\n{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(data['topic_name'],data['channel_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
            elif a == 2:
                for d in range(4):
                    # NSQ BINTARO
                    if d == 3 : req     = requests.get(self.apinsq['api_nsq_bint'])
                    # NSQ NDC
                    else: req = requests.get(self.apinsq['api_nsq_ndc_profiling'].format(self.fromnsqph[d]))
                    data    = json.loads(req.content)
                    conektion   = len(data['channels'][0]['clients'])
                    depth   = data['depth']
                    if depth != 0:
                        self.botmesseg = "*\n{0}*\ndepth : {1}\nconnection : {2}\nchannel_name : profiling-parser".format(data['topic_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
                else: break
            a += 1
        return judul,result

    # def cekkafka(bot, update):
    #     chatid = update.message.chat_id
    #     bot.send_message(chat_id=chatid, tex)



if __name__ == "__main__":
    bot = telegram.Bot(token='901108226:AAGwV9OvgxUjE117kwS0PEWx-1R3vU7hvLk')
    textbean    = alert().frombeanstalk()
    textnsq = alert().fromnsq()
    textkafka = alert().fromkafka()
    finalres = []
    for z in range(3):
        if z == 1:
            finalres.append(textbean[0])
            for a in range(textbean[1].__len__()): finalres.append(textbean[1][a])
        elif z == 2:
            finalres.append(textkafka[0])
            for a in range(textkafka[1].__len__()):
                if textkafka[2][a].__len__() <= 0: continue
                else:
                    finalres.append(textkafka[1][a])
                    for b in range(textkafka[2][a].__len__()): finalres.append(textkafka[2][a][b])
        else:
            finalres.append(textnsq[0])
            if textnsq[1].__len__()== 0: finalres.append("\nNSQ Have no error.")
            else:
                for a in range(textnsq[1].__len__()): finalres.append(textnsq[1][a])
    print ('\n').join(finalres)
    bot.send_message(chat_id='-128878416', text=('\n').join(finalres),parse_mode=telegram.ParseMode.MARKDOWN)
    # bot.send_message(chat_id='-234134770', text=('\n').join(finalres), parse_mode=telegram.ParseMode.MARKDOWN)
    # updater = Updater('901108226:AAGwV9OvgxUjE117kwS0PEWx-1R3vU7hvLk')
    # dp = updater.dispatcher
    # dp.add_handler(CommandHandler('cekkafka',alert().cekkafka()))
    # updater.start_polling()
    # updater.idle()
    # print (textkafka)a
    bot.send_message(chat_id='555942060', text=('\n').join(finalres), parse_mode=telegram.ParseMode.MARKDOWN)
    # 555942060 - > kenzila chat id
    # -128878416 - > grup crawl ops chat id
    # -234134770 - >avenger reborn