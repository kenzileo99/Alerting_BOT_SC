from pystalk import BeanstalkClient
import re,requests,json, telegram

___author___  = "Kenj"

class alert:
    def __init__(self):
        self.ip_ph = '192.168.99.69'
        self.ip_bint    = '192.168.20.92'
        self.fromnsqph  = ['sc_instagram','sc_facebook','sc_youtube']
        self.fromnsqndc = ['online-news','printed-news','tv-news']

    def frombeanstalk(self,result = []):
        a   = 0
        judul   = "\n-- \t BEANSTALK \t --"
        while True:
            tw  = re.compile('sc_twitter_')
            fb  = re.compile('sc_fb_')
            ig  = re.compile('sc_ig_')
            yt  = re.compile('sc_youtube_')
            if a == 1:
                tube_list_fb    = []
                tube_list_ig    = []
                tube_list_yt    = []
                client  = BeanstalkClient(self.ip_ph, 11300)
                list_tube   = client.list_tubes()
                for b in range(list_tube.__len__()):
                    if fb.findall(list_tube[b]):
                        tube_list_fb.append(list_tube[b])
                    if ig.findall(list_tube[b]):
                        tube_list_ig.append(list_tube[b])
                    if yt.findall(list_tube[b]):
                        tube_list_yt.append(list_tube[b])
                tube_list   = tube_list_fb + tube_list_ig +  tube_list_yt
                for c in range(tube_list.__len__()):
                    data    = client.stats_tube(tube_list[c])
                    if data['current-jobs-urgent'] != 0 or data['current-jobs-ready'] != 0:
                        res = "\n{0}\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}".format(data['name'],
                                                                                                               data['current-watching'],
                                                                                                               data['current-jobs-urgent'],
                                                                                                               data['current-jobs-ready'],
                                                                                                               data['current-jobs-buried'])
                        result.append(res)
                    else:
                        continue
                break
            else:
                tube_list   = []
                client  = BeanstalkClient(self.ip_bint, 11300)
                list_tube   = client.list_tubes()
                for b in range(list_tube.__len__()):
                    # or fb.findall(list_tube[b]) or ig.findall(list_tube[b]) or yt.findall(list_tube[b]):
                    if tw.findall(list_tube[b]):
                        tube_list.append(list_tube[b])
                for c in range(tube_list.__len__()):
                    data    = client.stats_tube(tube_list[c])
                    if data['current-jobs-urgent'] != 0 or data['current-jobs-ready'] != 0:
                        res = "\n{0}\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}".format(data['name'],
                                                                                                                   data['current-watching'],
                                                                                                                   data['current-jobs-urgent'],
                                                                                                                   data['current-jobs-ready'],
                                                                                                                   data['current-jobs-buried'])
                        result.append(res)
                    else: continue
            a += 1
        return judul,result

    def fromnsq(self, result = []):
        a   = 0
        judul = "\n--\t NSQ \t--"
        while True:
            if a == 0:
                #NSQ PH
                for b in self.fromnsqph:
                    req = requests.get('http://192.168.99.68:4171/api/topics/{}/nsq_to_nsq'.format(b))
                    data    = json.loads(req.content)
                    depth       = data['depth']
                    conektion    = len(data['clients'])
                    if depth != 0:
                        self.botmesseg ="\ntopic_name: {0}\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(data['topic_name'],data['channel_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
            elif a == 1:
                #NSQ NDC
                for c in self.fromnsqndc:
                    req     = requests.get('http://192.168.150.156:4171/api/topics/{}/nsq-to-kafka-profiling'.format(c))
                    data = json.loads(req.content)
                    depth = data['depth']
                    conektion   = len(data['clients'])
                    if depth != 0:
                        self.botmesseg = "\ntopic_name : {0}\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(data['topic_name'],data['channel_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
            elif a == 2:
                #NSQ BINTARO
                for d in range(4):
                    if d == 3 :
                        req     = requests.get('http://192.168.20.92:4171/api/topics/sc_twitter')
                    else:
                        #NSQ NDC
                        req = requests.get('http://192.168.150.156:4171/api/topics/{}'.format(self.fromnsqph[d]))
                    data    = json.loads(req.content)
                    conektion   = len(data['channels'][0]['clients'])
                    depth   = data['depth']
                    if depth != 0:
                        # print "\ntopic_name : {0}\ndepth : {1}\nconnection : {2}\nchannel_name : profiling-parser".format(data['topic_name'],depth,conektion)
                        self.botmesseg = "\ntopic_name : {0}\ndepth : {1}\nconnection : {2}\nchannel_name : profiling-parser".format(data['topic_name'],depth,conektion)
                        result.append(self.botmesseg)
                    else: continue
                else:
                    break
            a += 1
        return judul,result

if __name__ == "__main__":
    bot = telegram.Bot(token='901108226:AAGwV9OvgxUjE117kwS0PEWx-1R3vU7hvLk')
    textbean    = alert().frombeanstalk()
    textnsq = alert().fromnsq()
    finalbean = []
    finalnsq    = []
    for z in range(2):
        if z == 1:
            finalbean.append(textbean[0])
            for a in range(textbean[1].__len__()):
                finalbean.append(textbean[1][a])
            # bot.send_message(chat_id = '-128878416', text = textbean[0])
            # for a in range(textbean[1].__len__()):
            #     bot.send_message(chat_id = '-128878416', text = textbean[1][a])
        else:
            finalbean.append(textnsq[0])
            if textnsq[1].__len__()== 0:
                finalbean.append("\nNSQ Have no error.\n")
            else:
                for a in range(textnsq[1].__len__()):
                    finalbean.append(textnsq[1][a])

    # print ('\n').join(finalbean)
    bot.send_message(chat_id='-128878416', text=('\n').join(finalbean))

