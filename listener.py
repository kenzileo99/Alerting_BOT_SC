# from telegram.ext import CommandHandler, Updater
import telebot
from pystalk import BeanstalkClient
from ConfigParser import ConfigParser
from bs4 import BeautifulSoup
import requests, re,json

bot = telebot.TeleBot('901108226:AAGwV9OvgxUjE117kwS0PEWx-1R3vU7hvLk')

def fromkafka():
    parser  = ConfigParser()
    parser.read("config.conf")
    resultfinal = []
    check_list = parser.get('kafka_consumer_list','list_checker').split(',')
    resultfinal.append("--\t*Kafka*\t--")
    for a in check_list:
        # print a
        result = []
        url = parser.get('kafka_consumer_list','url_source').format(a)
        # print url
        geturl  = requests.get(url)
        soup    = BeautifulSoup(geturl.content,'html.parser')
        get_consumer    = soup.find_all('tr')
        for b in range(get_consumer.__len__()-1):
            b+=1
            filter1 = re.sub('<.*?>','',str(get_consumer[b]))
            filter2 = re.sub(' ','',filter1)
            splitter = filter2.split("\n")
            for c in range(4):
                if splitter[c] is '':
                    splitter.pop(c)
                    continue
            # print splitter
            try:
                if int(splitter[2]) <= 0 : continue
                else:
                    result.append("{0} : {1}".format(splitter[0], splitter[2]))
            except:
                result.append("{0} : {1}".format(splitter[0], splitter[2]))


        if result.__len__() is 0 : continue
        else: resultfinal.append("\n*{0}*\n{2}\n{1}".format(a,"\n".join(result),url))
    if resultfinal.__len__() <= 1: resultfinal.append("Kafka Safe.")
    print "\n".join(resultfinal)
    return resultfinal

def fromnsq():
    parser = ConfigParser()
    parser.read('config.conf')
    result = []
    # print "nsq"
    PH = ['sc_instagram','sc_facebook','sc_youtube']
    NDC = ['online-news','printed-news','tv-news']
    sc_facebook_profiling = ['ipd-parser','nsq_to_nsq','parser']
    sna_facebook_comment = ['parser','request']
    sna_facebook_friend_x_group_member = ['ipd-parser','parser','test']
    sna_facebook_page = ['parser']
    sna_facebook_post = ['current','ipd-parser-facebook']
    sna_facebook_post_page = ['download','parser']
    sna_facebook_profile_picture = ['download','parser','parser-2']
    sna_facebook_profiling = ['ipd-parser','language','parser','test']
    sna_linkedin_gcse_advanced = ['parser']

    a = 0
    result.append("--\t*NSQ*\t--")
    while True:
        if a == 0:
            for b in PH:
                check_list = parser.get('api_nsq','api_nsq_ph').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                # conektion   = len(req['clients'])
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0:
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth,conektion)
                    result.append(botmesseg)
            # print "PH DONE"
        elif a == 1:
            for b in NDC:
                check_list = parser.get('api_nsq','api_nsq_ndc').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth   = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0:
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth,conektion)
                    result.append(botmesseg)
            # print "NDC DONE"
        elif a == 2:
            check_list = parser.get('api_nsq','api_nsq_bint')
            req = json.loads((requests.get(check_list)).content)
            depth   = req['depth']
            if req['clients'] is None: conektion = 'No clients connected to this channel'
            else: conektion = len(req['clients'])
            if depth != 0 :
                botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth,conektion)
                result.append(botmesseg)
        elif a == 3:
            for b in PH:
                check_list = parser.get('api_nsq','api_nsq_ndc_profiling').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth   = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth,conektion)
                    result.append(botmesseg)
        elif a == 4:
            for b in sc_facebook_profiling:
                check_list = parser.get('api_nsq','api_nsq_sc_facebook_profiling').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth,conektion)
                    result.append(botmesseg)
        elif a == 5:
            for b in sna_facebook_comment:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_comment').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 6:
            for b in sna_facebook_friend_x_group_member:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_friend').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 7:
            for b in sna_facebook_friend_x_group_member:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_group_member').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 8:
            for b in sna_facebook_page:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_page').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 9:
            for b in sna_facebook_post:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_post').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 10:
            for b in sna_facebook_post_page:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_post_page').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 11:
            for b in sna_facebook_profile_picture:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_profile_picture').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 12:
            for b in sna_facebook_profiling:
                check_list = parser.get('api_nsq','api_nsq_sna_facebook_profiling').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        elif a == 13:
            for b in sna_linkedin_gcse_advanced:
                check_list = parser.get('api_nsq','api_nsq_sna_linkedin_gcse_advanced').format(b)
                req = json.loads((requests.get(check_list)).content)
                depth = req['depth']
                if req['clients'] is None: conektion = 'No clients connected to this channel'
                else: conektion = len(req['clients'])
                if depth != 0 :
                    botmesseg = "\n*{0}*\ndepth : {2}\nconnection : {3}\nchannel_name : {1}".format(req['topic_name'],req['channel_name'],depth, conektion)
                    result.append(botmesseg)
        else: break
        a += 1
    if result.__len__() <= 1: result.append("\nNSQ Safe.")
    # print result
    print "\n".join(result).replace('_',' ')
    return result

def frombeanstalk():
    parser  = ConfigParser()
    result  = []
    parser.read('config.conf')
    a = 0
    result.append("--\t*Beanstalk*\t--")
    while True:
        tw = re.compile('sc_twitter_')
        fb = re.compile('sc_fb_')
        ig = re.compile('sc_ig_')
        yt = re.compile('sc_youtube_')
        if a == 1:
            tube_list_fb    = []
            tube_list_ig    = []
            tube_list_yt    = []

            client = BeanstalkClient(parser.get('ip_beanstalk','ip_ph'),parser.get('ip_beanstalk','port_ph'))
            list_tube = client.list_tubes()
            # print list_tube
            for b in range(list_tube.__len__()):
                #     if tw.find
                if fb.findall(list_tube[b]):
                    tube_list_fb.append(list_tube[b])
                if ig.findall(list_tube[b]):
                    tube_list_ig.append(list_tube[b])
                if yt.findall(list_tube[b]):
                    tube_list_yt.append(list_tube[b])
            tube_list   = tube_list_fb + tube_list_ig + tube_list_yt
            # print tube_list
            for c in tube_list:
                data = client.stats_tube(c)
                if data['current-jobs-urgent'] >= 3000 or data['current-jobs-ready'] >= 3000:
                    res = "\n*{0}*\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}\njobs-reserved: {5}" \
                        .format(data['name'], data['current-watching'], data['current-jobs-urgent'],
                                data['current-jobs-ready'], data['current-jobs-buried'], data['current-jobs-reserved'])
                    result.append(res)
                else: continue
            # break
        elif a == 0 :
            tube_list = []
            client  = BeanstalkClient(parser.get('ip_beanstalk','ip_bintaro'),parser.get('ip_beanstalk','port_bintaro'))
            list_tube   = client.list_tubes()
            for b in range(list_tube.__len__()):
                if tw.findall(list_tube[b]): tube_list.append(list_tube[b])
            for c in tube_list:
                data    = client.stats_tube(c)
                if data['current-jobs-urgent'] >= 3000 or data['current-jobs-ready'] >= 3000:
                    res = "\n*{0}*\nworker : {1}\njobs-urgent : {2}\njobs-ready : {3}\njobs-buried : {4}\njobs-reserved: {5}" \
                        .format(data['name'], data['current-watching'], data['current-jobs-urgent'],
                                data['current-jobs-ready'], data['current-jobs-buried'], data['current-jobs-reserved'])
                    result.append(res)
                else: continue
        else: break
        a+=1
    if result.__len__() <= 1: result.append("\nBeanstalk Safe.")
    print "\n".join(result)
    return result

def kafka():
    parser  = ConfigParser()
    parser.read("config.conf")
    resultfinal = []
    check_list = parser.get('kafka_consumer_list','list_checker').split(',')
    resultfinal.append("--\t*Kafka*\t--")
    for a in check_list:
        # print a
        result = []
        url = parser.get('kafka_consumer_list','url_source').format(a)
        # print url
        geturl  = requests.get(url)
        soup    = BeautifulSoup(geturl.content,'html.parser')
        get_consumer    = soup.find_all('tr')
        for b in range(get_consumer.__len__()-1):
            b+=1
            filter1 = re.sub('<.*?>','',str(get_consumer[b]))
            filter2 = re.sub(' ','',filter1)
            splitter = filter2.split("\n")
            for c in range(4):
                if splitter[c] is '':
                    splitter.pop(c)
                    continue
            # print splitter
            try:
                if int(splitter[2]) <= 0 : continue
                else:
                    result.append("{0} : {1}".format(splitter[0], splitter[2]))
            except:
                result.append("{0} : {1}".format(splitter[0], splitter[2]))

        if result.__len__() is 0 : continue
        else: resultfinal.append("\n{0}".format("\n".join(result)))
    if resultfinal.__len__() <= 1: resultfinal.append("Kafka Safe.")
    print "\n".join(resultfinal)
    return resultfinal

@bot.message_handler(commands=['cekkafka'])
def cekkafka(message):
    # chatid = update.message.chat_id
    try:
        bot.reply_to(message, "\n".join(fromkafka()), parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, "Error!! : {}".format(e))

@bot.message_handler(commands=['cekbeanstalk'])
def cekbeanstalk(message):
    # chatid  = update.message.chat_id
    try:
        bot.reply_to(message, "\n".join(frombeanstalk()), parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, "Error!! : {}".format(e))

@bot.message_handler(commands=['ceknsq'])
def ceknsq(message):
    # chatid = update.message.chat_id
    try:
        bot.reply_to(message, "\n".join(fromnsq()).replace('_',' '), parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, "Error!! : {}".format(e))

@bot.message_handler(commands=['kafkares'])
def kafkaa(message):
    try:
        bot.reply_to(message, "\n".join(kafka()), parse_mode='markdown')
    except Exception as e:
        bot.reply_to(message, "Error!! : {}".format(e))
    return kafka()

if __name__ == '__main__':
    print "Running .."
    try:
        bot.polling()
    except Exception as e:
        bot.polling()
    # fromnsq()
    # frombeanstalk()
    # kafkaa()
    # main()