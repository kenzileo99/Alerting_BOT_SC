FROM python:2.7-alpine3.7 AS base
COPY requirements.txt /
WORKDIR /
RUN apk add --no-cache --virtual .build-deps gcc python2-dev \
&& apk add musl-dev libffi-dev  \
&& apk del libressl-dev \
&& apk add openssl-dev \
&& pip install cryptography==2.2.2 \
&& apk del openssl-dev \
&& apk add libressl-dev\
&& pip install --no-cache-dir -r requirements.txt \
&& apk del .build-deps

FROM base
COPY ./ /Alerting
WORKDIR /Alerting
ENTRYPOINT ["python"]
